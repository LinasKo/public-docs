# Overview
This holds various public documents about me. Primarily career-related.

# Attribution
Repo icon made by [Freepik](https://www.flaticon.com/free-icons/portfolio)